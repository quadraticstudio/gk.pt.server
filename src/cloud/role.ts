import * as Model from '../model';
import initialize from '../initialize';
Parse.Cloud.beforeSave(Model.Role, async (req, res) => {
  req = initialize.initAcl(req);
  res.success();
});
