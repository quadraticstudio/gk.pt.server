import * as Model from '../model';
import initialize from '../initialize';

Parse.Cloud.beforeSave(Model.User, async (req, res) => {
  req = initialize.initAcl(req);
  res.success();
});