import * as xlsx from 'xlsx-template';
import { LINQ } from 'berish-linq';
import * as fs from 'fs';
import * as Model from '../../model';
import { getResourceFileUrl } from '../../abstract/resource';

interface IExcelModelColumn {
  team: string;
  position: string;
  name: string;
  task: string;
  phoneCode: string;
  phoneMobile: string;
  officeNumber: string;
  floorOrRoom: string;
  projects: string[];
}

interface IExcelModel {
  items: IExcelModelColumn[];
  titles: IExcelModelColumn[];
}

function readFile(url: string) {
  return new Promise<Buffer>((resolve, reject) =>
    fs.readFile(url, (err, data) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(data);
    })
  );
}

Parse.Cloud.define('excel:teamUser', async (req, res) => {
  const projects = await new Parse.Query(Model.Project).limit(1000).find({ useMasterKey: true });
  let mProjects = projects.filter(m => m.relType == Model.ProjectRelTypeEnum.M);
  let moProjects = projects.filter(m => m.relType == Model.ProjectRelTypeEnum.MO);
  const data = await new Parse.Query(Model.TeamUser)
    .include(['projects', 'team'])
    .limit(1000)
    .find({ useMasterKey: true });

  let titles: IExcelModelColumn = {
    team: 'Отдел',
    position: 'Проектная роль',
    name: `ФИО (${data.length})`,
    task: 'Блок задач',
    phoneCode: 'Доб.',
    phoneMobile: 'Мобильный',
    officeNumber: 'Оф.',
    floorOrRoom: 'Эт/Каб.',
    projects: ['М', ...mProjects.map(m => m.code), 'МО', ...moProjects.map(m => m.code)]
  };
  let items: IExcelModelColumn[] = data.map(m => {
    let linqProjects = LINQ.fromArray(m.projects);
    let mProjectsColumns = mProjects.map(k => {
      let item = linqProjects.firstOrNull(l => l.id == k.id);
      return item ? item.code : '';
    });
    let moProjectsColumns = moProjects.map(k => {
      let item = linqProjects.firstOrNull(l => l.id == k.id);
      return item ? item.code : '';
    });
    let item: IExcelModelColumn = {
      team: (m.team && m.team.name) || '-',
      position: m.position || '-',
      name: `${m.lastname || ''} ${m.name || ''} ${m.patronymic || ''}`.trim() + ` (${m.projects.length})`,
      task: m.task || '-',
      phoneCode: (m.phoneCode && `${m.phoneCode}`) || '-',
      phoneMobile: m.phoneMobile || '-',
      officeNumber: m.officeNumber || '-',
      floorOrRoom: m.floorOrRoom || '-',
      projects: [
        linqProjects.any(k => k.relType == Model.ProjectRelTypeEnum.M) ? 'M' : '',
        ...mProjectsColumns,
        linqProjects.any(k => k.relType == Model.ProjectRelTypeEnum.MO) ? 'МО' : '',
        ...moProjectsColumns
      ]
    };
    return item;
  });

  const model: IExcelModel = { items, titles: [titles] };
  const templateFile = await readFile(getResourceFileUrl('template.xlsx'));
  const template = new xlsx(templateFile);
  const sheetNumber = 1;
  template.substitute(sheetNumber, model);

  const outputData: Buffer = template.generate({ type: 'base64' });
  return outputData.toString();
});
