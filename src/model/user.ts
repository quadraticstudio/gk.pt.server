export class User extends Parse.User {
    constructor() {
        super('_User')
    }
}

Parse.Object.registerSubclass('_User', User);