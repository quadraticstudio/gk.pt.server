export class Team extends Parse.Object {
    constructor(){
        super('Team')
    }

    get name() {
        return this.get('name')
    }

    set name(value: string) {
        this.set('name', value)
    }
}

Parse.Object.registerSubclass('Team', Team);