export class Role extends Parse.Role {
    constructor() {
        super('_Role', null);
    }
}

Parse.Object.registerSubclass('_Role', Role);