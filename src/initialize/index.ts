import * as ringle from 'berish-ringle';

class Initializer {

  initAcl(req: Parse.Cloud.BeforeSaveRequest) {
    let user = req.user;
    let object = req.object;
    if (user) {
      let acl = user.getACL();
      object.setACL(acl);
    }
    return req;
  }
}

export default ringle.getSingleton(Initializer);
