import general from './general';
import parse from './parse';
import dashboard from './dashboard';
export default { general, parse, dashboard };
