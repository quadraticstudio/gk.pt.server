import * as path from 'path';

export default {
  startPage: 'index.html',
  public: path.normalize(path.join(__dirname, '../../../public')),
  useSSL: false
};
