import base from './base';
import { IParseConfig } from '../interfaces/parse';

export default {
  ...base,
  serverURL: `http://127.0.0.1:${base.port}${base.mountPath}`,
  // databaseURI: 'mongodb://localhost:27017/gk-pt-db',
  publicServerURL: `http://207.154.239.144:${base.port}${base.mountPath}`,
  production: true
} as IParseConfig;
