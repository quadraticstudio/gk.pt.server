import { IParseConfig } from '../interfaces/parse';
import * as path from 'path';

const obj: IParseConfig = {
  mountPath: '/ingradapi',
  appName: 'INGRAD Project Team server',
  cloud: path.normalize(path.join(__dirname, '../../cloud/index.js')),
  databaseURI: 'mongodb://admin:4X4ruLjk6k5UvDNt@ds042687.mlab.com:42687/gk-pt-db',
  appId: 'xhdbw4jfek4n2hh2dc87wdctnzugbdj8',
  masterKey: 'vbpcvvaa673shzphyz568ypvhnptse6j',
  clientKey: 'rgbxw6curxxc2gvxkdgpux6wd64au54z',
  javascriptKey: 'x8zqswgd3zv9k7q6ymuk5wn5h6pr76x5',
  logLevel: 'error',
  port: 9696
};
export default obj;
