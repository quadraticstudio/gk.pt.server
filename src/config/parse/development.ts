import base from './base';
import { IParseConfig } from '../interfaces/parse';
export default {
  ...base,
  serverURL: `http://127.0.0.1:${base.port}${base.mountPath}`,
  publicServerURL: `http://127.0.0.1:${base.port}${base.mountPath}`
} as IParseConfig;
