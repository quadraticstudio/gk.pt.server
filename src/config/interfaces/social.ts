export interface IFacebookAuthConfig {
  appIds: string;
}
export interface ITwitterAuthConfig {
  consumer_key: string;
  consumer_secret: string;
}
export interface IVKAuthConfig {
  appSecret: string;
  appIds: string;
}
