import {
  IVKAuthConfig,
  IFacebookAuthConfig,
  ITwitterAuthConfig
} from './social';

export interface IParseConfig {
  appId?: string;
  masterKey?: string;
  databaseURI?: string;
  cacheAdapter?: any;
  port?: number;
  cloud?: string;
  serverURL?: string;
  masterKeyIps?: string[];
  appName?: string;
  clientKey?: string;
  javascriptKey?: string;
  restAPIKey?: string;
  dotNetKey?: string;
  fileKey?: string;
  allowClientClassCreation?: boolean;
  enableAnonymousUsers?: boolean;
  auth?: {
    vkontakte?: IVKAuthConfig;
    facebook?: IFacebookAuthConfig;
    twitter?: ITwitterAuthConfig;
  };
  facebookAppIds?: string[];
  mountPath?: string;
  filesAdapter?: string;
  maxUploadSize?: number;
  loggerAdapter?: string;
  logLevel?:
    | 'emerg'
    | 'alert'
    | 'crit'
    | 'error'
    | 'warning'
    | 'notice'
    | 'info'
    | 'debug';
  sessionLength?: number;
  maxLimit?: any;
  revokeSessionOnPasswordReset?: boolean;
  accountLockout?: {
    duration: number;
    threshold: number;
  };
  passwordPolicy?: {
    validatorPattern: RegExp;
    validatorCallback: (password: string) => boolean;
    doNotAllowUsername: boolean;
    maxPasswordAge: number;
    maxPasswordHistory: number;
    resetTokenValidityDuration: number;
  };
  customPages?: {
    parseFrameURL?: string;
    invalidLink?: string;
    choosePassword?: string;
    passwordResetSuccess?: string;
    verifyEmailSuccess?: string;
  };
  readOnlyMasterKey?: boolean;
  objectIdSize?: number;
  verifyUserEmails?: boolean;
  emailVerifyTokenValidityDuration?: number;
  preventLoginWithUnverifiedEmail?: boolean;
  publicServerURL?: string;
  emailAdapter?: {
    module: string;
    options: { [key: string]: any };
  };
  production?: boolean;
}
