export const guidName = "67ad7b2a-7917-4bac-ab63-54c9643f527c";
export const top = 10000;
export const fields = [
  "ID",
  "LinkTitle",
  "Login",
  "Firstname",
  "Lastname",
  "Patronymic",
  "Photo",
  "Position",
  "PhoneWork",
  "PersonalPhone",
  "Email",
  "IDOneC",
  "SNILS",
  "INN",
  "Organization",
  "CurrentEmployee",
  "Interests",
  "Sphere",
  "Office",
  "Sex",
  "PersonalEmail",
  "OtherContact",
  "Media",
  "articles",
  "Skills",
  "PhoneWorker",
  "PhoneAdditional",
  "DepartmentId",
  "IsArchive",
  "HireDate",
  "Birthday",
  "hidetel",
  "DisplayOnPortal",
  "FloorNumber"
];

export interface Model {
  Id: number;
  Login: string;
  Firstname: string;
  Lastname: string;
  Patronymic: string;
  Photo: string;
  Position: string;
  PhoneWork: string;
  PersonalPhone: string;
  Email: string;
  IDOneC: string;
  SNILS: string;
  INN: string;
  Organiation: string;
  CurrentEmployee: string;
  Interests: string;
  Sphere: string;
  Office: string;
  Sex: "F" | "M";
  PersonalEmail: string;
  OtherContact: string;
  Media: string;
  articles: string;
  Skills: string;
  PhoneWorker: string;
  PhoneAdditional: string;
  DepartmentId: number;
  IsArchive: boolean;
  HireDate: string;
  Birthday: string;
  hidetel: boolean;
  DisplayOnPortal: string;
  FloorNumber: number;
  LinkTitle: string;
}
