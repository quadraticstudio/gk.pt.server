import * as ringle from "berish-ringle";
import * as Parse from "parse/node";
import { LINQ } from "berish-linq";
import apiController from "./controllers/apiController";

import * as Model from "./model";
import spController from "./sp/spController";
import configController from "./controllers/configController";
import * as cron from "node-cron";

import config from "../../config/modules/sp-cron";

class SPApp {
  get controllers() {
    return {
      configController,
      apiController
    };
  }

  init() {
    apiController.init();
  }

  async run() {
    // Get Parse Model
    let users = await spController.getParseUsers();
    let orgs = await spController.getParseOrgStructures();
    let usersIds = users.select(m => m.spID);
    let orgsIds = orgs.select(m => m.spID);

    // Get Sharepoint Model
    let spUsers = await spController.getUsers();
    let spOrgs = await spController.getOrgStructures();
    let spUsersIds = spUsers.select(m => m.Id);
    let spOrgsIds = spOrgs.select(m => m.Id);

    // Get Model for update from SP to Parse
    let usersUpdatetIds = spUsersIds.intersect(usersIds);
    let orgsUpdateIds = spOrgsIds.intersect(orgsIds);

    // Get Model for add from SP to Parse
    let usersAddIds = spUsersIds.except(usersIds);
    let orgsAddIds = spOrgsIds.except(orgsIds);

    // Get Model for remove from Parse, because they re not in SP
    let usersRemoveIds = usersIds.except(spUsersIds);
    let orgsRemoveIds = orgsIds.except(spOrgsIds);

    const orgsAddSave = orgsAddIds.select(id => {
      let model = spOrgs.firstOrNull(m => m.Id == id);
      return spController.toSPOrgStructure(model, new Model.SPOrgStructure());
    });

    const orgsUpdateSave = orgsUpdateIds.select(id => {
      let model = spOrgs.firstOrNull(m => m.Id == id);
      let parseModel = orgs.firstOrNull(m => m.spID == id);
      return spController.toSPOrgStructure(model, parseModel);
    });

    let orgsSave = orgsAddSave.concat(orgsUpdateSave);
    let orgsSaveParse = await Parse.Object.saveAll(orgsSave.toArray());
    orgsSave = LINQ.fromArray(orgsSaveParse);

    let orgsRemove = orgsRemoveIds
      .select(id => {
        return orgs.firstOrNull(m => m.spID == id);
      })
      .toArray();

    orgsRemove = await Parse.Object.destroyAll(orgsRemove);

    const usersAddSave = usersAddIds.select(id => {
      const from = spUsers.firstOrNull(m => m.Id == id);
      const departament = orgsSave.firstOrNull(
        m => m.spID == from.DepartmentId
      );
      return spController.toSPUserModel(
        { from, departament },
        new Model.SPUser()
      );
    });

    const usersUpdateSave = usersUpdatetIds.select(id => {
      const from = spUsers.firstOrNull(m => m.Id == id);
      const departament = orgsSave.firstOrNull(
        m => m.spID == from.DepartmentId
      );
      const parseModel = users.firstOrNull(m => m.spID == id);
      return spController.toSPUserModel({ from, departament }, parseModel);
    });

    let usersSave = usersAddSave.concat(usersUpdateSave).toArray();
    usersSave = await Parse.Object.saveAll(usersSave);

    let usersRemove = usersRemoveIds
      .select(id => {
        return users.firstOrNull(m => m.spID == id);
      })
      .toArray();

    usersRemove = await Parse.Object.destroyAll(usersRemove);
  }
}

const app = ringle.getSingleton(SPApp);
app.controllers.configController.init(config);
app.init();

const task = cron.schedule("*/2 * * * *", async () => {
  console.log("start cron");
  await app.run();
  console.log("end cron");
});
task.start();
