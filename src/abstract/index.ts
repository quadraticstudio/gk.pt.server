import Logger from './logger';
import * as resource from './resource';
import * as server from './server';

export default {
  Logger,
  resource,
  server
};
