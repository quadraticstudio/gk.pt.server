import * as http from 'http';
import * as https from 'https';
import * as Express from 'express';
import { ParseServer } from 'parse-server';
import * as bodyParser from 'body-parser';
import * as path from 'path';
import config from '../config';
import { IParseConfig } from '../config/interfaces/parse';
import getLogger, { LoggerTypeEnum } from './logger';

export function createServer(express: Express.Express) {
  if (config.general.express.useSSL) return https.createServer({}, express);
  else return http.createServer(express);
}

export function createParseServer(parseConfig: IParseConfig) {
  return new ParseServer(parseConfig);
}

export function configurateServer(
  expressApplication: Express.Express,
  parseServer,
  parseDashboard
) {
  expressApplication.use('/', Express.static(config.general.express.public));
  expressApplication.use(config.dashboard.mount, parseDashboard);
  expressApplication.use(bodyParser.json());
  expressApplication.use(bodyParser.urlencoded({ extended: true }));
  expressApplication.get('/', (req, res) => {
    getLogger(LoggerTypeEnum.SERVER).info(`${req.method} ${req.url}`);
    res
      .status(200)
      .sendFile(
        path.join(
          config.general.express.public,
          config.general.express.startPage
        )
      );
  });
  expressApplication.use(config.parse.mountPath, parseServer);
}
